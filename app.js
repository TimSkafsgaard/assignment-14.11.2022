const balanceElement = document.getElementById("balance");
const loanButtonElement = document.getElementById("loanButton");
const bankMoneyButton = document.getElementById("bankMoneyButton")
const workButton = document.getElementById("workButton")
const walletElement = document.getElementById("wallet");
const computersElement = document.getElementById("computers");
const featuresElement = document.getElementById("features");
const computerImageElement = document.getElementById("computerImage").src;
const computerTitleElement = document.getElementById("computerTitle");
const computerDescriptionElement = document.getElementById("computerDescription");
const priceElement = document.getElementById("price");
const buyNowButtonElement = document.getElementById("buyNowButton");

let computers = [];
let wallet = 0.0;
let balance = 0.0;
let price = 0.0;

fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(Response => Response.json())
    .then(data => computers = data)
    .then(computers => addComputersToMeny(computers))
    .then(computers => addComputerImage(computers))

const addComputersToMeny = (computers) => {
    computers.forEach(x => addComputerToMeny(x));
    featuresElement.innerText = computers[0].specs;
    computerTitleElement.innerText = computers[0].title;
    computerDescriptionElement.innerText = computers[0].description;
    priceElement.innerText = computers[0].price;
}

const addComputerToMeny = (computer) => {
    const computerElement = document.createElement("option");
    computerElement.value = computer.id;
    computerElement.appendChild(document.createTextNode(computer.title));
    computersElement.appendChild(computerElement);
}

const handleComputerMenyChange = e => {
    const selectedComputer = computers[e.target.selectedIndex];
    featuresElement.innerText = selectedComputer.specs;
    computerTitleElement.innerText = selectedComputer.title;
    computerDescriptionElement.innerText = selectedComputer.description;
    priceElement.innerText = selectedComputer.price;
}

const addMoneyToWallet = e => {
    walletElement.innerText = (wallet += 100);
}

const addMoneyToBank = e => {
    balanceElement.innerText = (balance += wallet);
    walletElement.innerText = (0)
    wallet = 0;

}

const getLoan = e => {
    balanceElement.innerText = (balance *= 1.2)
}

computersElement.addEventListener("change", handleComputerMenyChange)
workButton.addEventListener("click", addMoneyToWallet)
bankMoneyButton.addEventListener("click", addMoneyToBank)
loanButtonElement.addEventListener("click", getLoan)